## A. Autentikasi
Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.

<b>1. Membedakan sudo dan user biasa

```
int main(int argc, char* argv[]) {
    if (geteuid() == 0){
    ......
    }
    else{
    ......
    }
}
```
- Dengan menggunakan getuid().
- Jika uid = 0 maka yang mengakses program tersebut adalah sudo. Jika selain itu, maka yang mengakses program tersebut adalah user biasa.

<b> 2. Register

Format :
> CREATE USER [nama_user] IDENTIFIED BY [password_user];


Solusi :
```
    if (geteuid() == 0){

    fgets(input, sizeof(input), stdin);

    // Memeriksa format perintah
    if (sscanf(input, "CREATE USER %s IDENTIFIED BY %s", nama_user, password_user) == 2) {
	registerUser(nama_user, password_user);
    }
    }
```
- Untuk main(), karna yang bisa register hanya sudo, maka getuidnya harus = 0.
- Kemudian user input command sesuai format.
- Command inputan user kemudian di scan, dan jika benar sesuai, maka akan diambil nama dan password user.
- Terakhir, memanggil fungsi registerUser() untuk register.

Fungsi registerUser()
```
    FILE *file = fopen("login.txt", "a");

    // Menghitung panjang password
    size_t password_length = strlen(password_user);

    // Memastikan password tidak kosong
    if (password_length > 0) {
        password_length--; // Menghapus karakter ';'
    }

    // Menulis nama pengguna dan kata sandi ke dalam file dengan format yang diinginkan
    fprintf(file, "%s:%.*s\n", nama_user, (int)password_length, password_user);

    // Menutup file
    fclose(file);

    return 0;

```
- Membuka file login.txt
- Kemudian password akan dihitung lengthnya. Selanjutnya password_length dikurangi 1 agar ';' hilang.
- Kemudian nama dan password user diprint ke login.txt dengan format "<username\>:<password\>"

<b> 3. Login

Format :
>./[program_client_database] -u [username] -p [password]

Contoh :
> ./client_databaseku -u khonsu -p khonsu123

Solusi :
```
    for (int i = 1; i < argc; i += 2) {
        if (strcmp(argv[i], "-u") == 0) {
            username = argv[i + 1];
        } else if (strcmp(argv[i], "-p") == 0) {
            password = argv[i + 1];
        }
    }
    	loginUser(username, password);
```
- Mengambil username dan password inputan user dengan cara memeriksa argumen -u dan -p.
- Setelah username dan password telah didapatkan, dipanggil function loginUser() untuk user login.

loginUser()
```
    FILE *file = fopen("login.txt", "r");

    char buffer[100];

    // Memeriksa kecocokan username dan password dalam file login.txt
    while (fgets(buffer, sizeof(buffer), file)) {
        char *saved_username;
        char *saved_password;

        saved_username = strtok(buffer, ":");
        saved_password = strtok(NULL, ":\n");

        if (strcmp(username, saved_username) == 0 && strcmp(password, saved_password) == 0) {
	    BerhasilLogin = 1;
            printf("Berhasil Login!\n");
            fclose(file);
            return 0;
        }
    }

    printf("Username atau password salah.\n");
    fclose(file);
    return 1;
```
- Membuka file login.txt
- Diambil username dan password dari setiap line dalam login.txt dengan cara memeriksa setiap line menggunakan fgets.
- Setelah username dan password dari login.txt diambil, kemudian dicocokkan dengan yang inputan user menggunakan strcmp.
- Jika cocok, maka variabel BerhasilLogin dijadikan 1.

## B. Autorisasi
<b> 1. Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.

Format :
> USE [nama_database];

Solusi :
```
    if(BerhasilLogin == 1){

        fgets(input, sizeof(input), stdin);

        if (sscanf(input, "USE %s", database) == 1) {
            useData(username, database);
        }
```
- Jika user berhasil login, user akan diminta untuk menginputkan command lagi.
- program akan melakukan check apakah user menginputkan command USE.
- Jika ya, akan dipanggil function useData() untuk mengakses suatu database.

useData()
```

    char query[MAX_LENGTH];
	char newdatabase[MAX_LENGTH];
	char table[MAX_LENGTH];
	char column[MAX_LENGTH];
        // Membuka file login.txt
        FILE *file = fopen("login.txt", "r");
        if (file != NULL) {

             char line[MAX_LENGTH];
	     size_t len = strcspn(database, ";");
    	     database[len] = '\0';

             // Membaca setiap baris dalam file
             while (fgets(line, sizeof(line), file)) {
                 // Memisahkan username dan database dalam baris
                 char *fileUsername = strtok(line, ";");
                 char *fileDatabase = strtok(NULL, "\n");

                 // Memeriksa apakah username dan database cocok
                 if (fileUsername != NULL && fileDatabase != NULL &&
                     strcmp(username, fileUsername) == 0 && strcmp(database, fileDatabase) == 0) {

                     ....

                     fclose(file);
                     return 0;
                 }
             }

             fclose(file);
         }

```
- Membuka login.txt untuk proses pengecekan akses user.
- Membaca setiap baris di login.txt sekaligus mengambil username dan database jika baris memiliki format "<username\>;<database\>".
- Setelah diambil, akan dicocokkan dengan yang diinputkan oleh user, menggunakan strcmp.

<b> 2. Yang bisa memberikan permission atas database untuk suatu user hanya root.

Format :
> GRANT PERMISSION [nama_database] INTO [nama_user];

Solusi :
```
    if (geteuid() == 0){
        ....
    else if (sscanf(input, "GRANT PERMISSION %s INTO %s", database, nama_user) == 2) {
        grantPermission(database, nama_user);
    }

```
- Karena yang bisa grant permission hanya root/sudo, maka uid harus == 0.
- Kemudian akan dicek dan diambil username dan database dari command inputan user, jika sesuai maka akan dipanggil function grantPermission(database, nama_user)

grantPermission(database, nama_user)
```
    // Membuka file login.txt dalam mode append ("a")
    FILE* file = fopen("login.txt", "a");

    size_t len = strcspn(user, ";");
    user[len] = '\0';

    // Menambahkan nama_database ke nama_user dalam format yang diinginkan
    fprintf(file, "%s;%s\n", user, database);

    // Menutup file
    fclose(file);

    printf("Perizinan berhasil ditambahkan.\n");
```
- Membuka login.txt untuk nanti dipakai menambahkan akses user.
- Char ';' akan dihilangkan dari username dengan menghitung length dari username dan length terakhir dijadikan '\0'.
- Jika ';' sudah hilang, akan ditambahkan akses ke login.txt dengan format "<username\>;<database\>".

## C. Data Definition Language
<b> Create database

Solusi createdatabase()
```
    // Membuka file login.txt dalam mode append ("a")
    FILE* file = fopen("login.txt", "a");

    size_t len = strcspn(database, ";");
    database[len] = '\0';

    // Menambahkan nama_database ke nama_user dalam format yang diinginkan
    fprintf(file, "%s;%s\n", user, database);

    // Menutup file
    fclose(file);

    // Create directory using system command
    char command[100];
    sprintf(command, "mkdir ./databases/%s", database);
    int status = system(command);
```
- Pertama, membuka login.txt untuk menambahkan akses user ke database yang dibuat dengan format "<username\>;<database\>".
- Jika akses sudah dibuat, Selanjutnya membuat direktori sebagai databasenya. dengan path "./databases/<nama database\>".

<b> 2. Create table

int createTable(const char* query, char* database)
```
    char tableName[MAX_LENGTH];

    char *openingBracket = strchr(query, '(');
    char *closingBracket = strchr(query, ')');

    char *tableNameStart, *tableNameEnd;

    // Find the start of the table name
    tableNameStart = strstr(query, "TABLE") + strlen("TABLE") + 1;

    // Find the end of the table name
    tableNameEnd = strchr(tableNameStart, ' ');

    // Copy the table name into the tableName variable
    strncpy(tableName, tableNameStart, tableNameEnd - tableNameStart);
    tableName[tableNameEnd - tableNameStart] = '\0'; // Null-terminate the string

    // Create the file
    char path[200];
    sprintf(path, "./databases/%s/%s", database, tableName);
    FILE* file = fopen(path, "w");
    if (file == NULL) {
        printf("Failed to create the file.\n");
        return 1;
    }
    char *columns = openingBracket + 1;
    columns[strcspn(columns, ")")] = '\0';

    fprintf(file, "%s\n", columns);
    fclose(file);

    printf("Table %s created successfully in database %s.\n", tableName, database);
```
- Akan dicek nama dari tabel yang diinputkan oleh user.
- Setelah nama tabel didapatkan, file <nama tabel\> dibuat di path sesuai database yang diinputkan user.
- Di dalam tabel, ditambahkan column-column dengan format "<column1\> <data type\>, <column2\> <data type\>, ....".

<b> 3. DROP database

void dropDatabase()
```
    char command[MAX_LENGTH + 10];
    sprintf(command, "rm -r databases/%s", databaseName);
    system(command);
    printf("Database %s dihapus.\n", databaseName);
```
- remove direktori database dengan nama database sesuai dengan yang diinputkan oleh user.

<b> 4. Drop table

void dropTable()
```
    size_t len = strcspn(tableName, ";");
    tableName[len] = '\0';
    char command[MAX_LENGTH + 10];
    sprintf(command, "rm ./databases/%s/%s", database, tableName);
    system(command);
    printf("Tabel %s dihapus.\n", tableName);
```
- Menghapus ';' agar nama tabel valid.
- Kemudian diremove file tabel dalam database yang user inputkan.

<b> 5. Drop Column

void dropColumn()
```
    size_t len = strcspn(tableName, ";");
    tableName[len] = '\0';

    // Membangun jalur file
    char path[256];
    sprintf(path, "./databases/%s/%s", database, tableName);

    // Membuka file untuk dibaca
    FILE* file = fopen(path, "r");

    // Membaca konten file ke dalam buffer
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* buffer = malloc(file_size + 1);
    fread(buffer, file_size, 1, file);
    buffer[file_size] = '\0';
    fclose(file);

    // Mencari posisi awal dan akhir kolom yang akan dihapus
    char* start = strstr(buffer, columnName);
    if (start == NULL) {
        printf("Kolom %s tidak ditemukan dalam tabel %s.\n", columnName, tableName);
        free(buffer);
        return;
    }
    char* end = strchr(start, ',');
    if (end == NULL) {
        end = strchr(start, '\0');
    }

    // Menghapus kolom dari buffer
    memmove(start, end + 1, strlen(end + 1) + 1);

    // Menulis kembali konten buffer ke dalam file
    file = fopen(path, "w");
    if (file == NULL) {
        printf("Tidak dapat membuka file %s untuk ditulis.\n", path);
        free(buffer);
        return;
    }
    fwrite(buffer, strlen(buffer), 1, file);
    fclose(file);

    free(buffer);
```

- Fungsi ini membatasi tableName dengan menghapus karakter ';' dan menggantinya dengan '\0'.
- Selanjutnya, membangun path menggunakan database dan table, kemudian membuka file tabel tersebut untuk dibaca.
- Isi file kemudian dibaca ke dalam buffer.
- Mencari posisi awal dan akhir kolom yang akan dihapus dalam buffer.
- Setelah itu, fungsi menghapus kolom dari buffer menggunakan memmove.
- Isi buffer yang telah diubah kemudian ditulis kembali ke dalam file.
- Terakhir, buffer dan file ditutup.

## E. Logging

Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.

1. void logCommand(), Untuk menambahkan log dengan format "timestamp(yyyy-mm-dd hh:mm:ss):username:command" ke dalam file command_log.txt.

```
    time_t rawtime;
    struct tm *timeinfo;
    char timestamp[20];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", timeinfo);

    FILE *logFile = fopen("command_log.txt", "a");
    if (logFile != NULL) {
        if (strcmp(username, "root") == 0) {
            fprintf(logFile, "%s:root:%s\n", timestamp, command);
        } else {
            fprintf(logFile, "%s:%s:%s\n", timestamp, username, command);
        }
        fclose(logFile);
    } else {
        printf("Failed to open log file.\n");
    }
```
- Pertama mengambil waktu saat ini menggunakan time_t.
- Kemudian command_log.txt dibuka.
- Terakhir, akan ditambahkan log text sesuai dengan user yang memakai command. Jika root/sudo maka yang ditambahkan ke dalam command_log.txt adalah "timestamp(yyyy-mm-dd hh:mm:ss):root:command" . Jika user biasa maka "timestamp(yyyy-mm-dd hh:mm:ss):username:command".
