#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LENGTH 100

int BerhasilLogin = 0;

int registerUser(const char* nama_user, const char* password_user) {
    FILE *file = fopen("login.txt", "a");

    if (file == NULL) {
        printf("Gagal membuka file.\n");
        return 1;
    }

    // Menghitung panjang password
    size_t password_length = strlen(password_user);

    // Memastikan password tidak kosong
    if (password_length > 0) {
        password_length--; // Menghapus karakter ';'
    } else {
        printf("Password tidak valid.\n");
        fclose(file);
        return 1;
    }

    // Menulis nama pengguna dan kata sandi ke dalam file dengan format yang diinginkan
    fprintf(file, "%s:%.*s\n", nama_user, (int)password_length, password_user);

    // Menutup file
    fclose(file);

    printf("Data pengguna telah disimpan di data.txt\n");

    return 0;
}

int loginUser(const char* username, const char* password) {
    FILE *file = fopen("login.txt", "r");
    if (file == NULL) {
        printf("Gagal membuka file login.txt.\n");
        return 1;
    }

    char buffer[100];

    // Memeriksa kecocokan username dan password dalam file login.txt
    while (fgets(buffer, sizeof(buffer), file)) {
        char *saved_username;
        char *saved_password;

        saved_username = strtok(buffer, ":");
        saved_password = strtok(NULL, ":\n");

        if (strcmp(username, saved_username) == 0 && strcmp(password, saved_password) == 0) {
	    BerhasilLogin = 1;
            printf("Berhasil Login!\n");
            fclose(file);
            return 0;
        }
    }

    printf("Username atau password salah.\n");
    fclose(file);
    return 1;
}

void grantPermission( char* database,  char* user) {
    // Membuka file login.txt dalam mode append ("a")
    FILE* file = fopen("login.txt", "a");

    if (file == NULL) {
        printf("Gagal membuka file login.txt.\n");
        exit(1);
    }
    size_t len = strcspn(user, ";");
    user[len] = '\0';

    // Menambahkan nama_database ke nama_user dalam format yang diinginkan
    fprintf(file, "%s;%s\n", user, database);

    // Menutup file
    fclose(file);

    printf("Perizinan berhasil ditambahkan.\n");
}

int createTable(const char* query, char* database) {

    char tableName[MAX_LENGTH];

    char *openingBracket = strchr(query, '(');
    char *closingBracket = strchr(query, ')');

    char *tableNameStart, *tableNameEnd;

    // Find the start of the table name
    tableNameStart = strstr(query, "TABLE") + strlen("TABLE") + 1;

    // Find the end of the table name
    tableNameEnd = strchr(tableNameStart, ' ');

    // Copy the table name into the tableName variable
    strncpy(tableName, tableNameStart, tableNameEnd - tableNameStart);
    tableName[tableNameEnd - tableNameStart] = '\0'; // Null-terminate the string

    // Create the file
    char path[200];
    sprintf(path, "./databases/%s/%s", database, tableName);
    FILE* file = fopen(path, "w");
    if (file == NULL) {
        printf("Failed to create the file.\n");
        return 1;
    }
    char *columns = openingBracket + 1;
    columns[strcspn(columns, ")")] = '\0';

    fprintf(file, "%s\n", columns);
    fclose(file);

    printf("Table %s created successfully in database %s.\n", tableName, database);
}

// Fungsi untuk menghapus file
void dropTable(char* tableName, char* database) {
    size_t len = strcspn(tableName, ";");
    tableName[len] = '\0';
    char command[MAX_LENGTH + 10];
    sprintf(command, "rm ./databases/%s/%s", database, tableName);
    system(command);
    printf("Tabel %s dihapus.\n", tableName);
}

// Fungsi untuk menghapus kolom
void dropColumn(char* columnName, char* tableName, char* database) {
    size_t len = strcspn(tableName, ";");
    tableName[len] = '\0';

    // Membangun jalur file
    char path[256];
    sprintf(path, "./databases/%s/%s", database, tableName);

    // Membuka file untuk dibaca
    FILE* file = fopen(path, "r");
    if (file == NULL) {
        printf("File %s tidak ditemukan.\n", path);
        return;
    }

    // Membaca konten file ke dalam buffer
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* buffer = malloc(file_size + 1);
    fread(buffer, file_size, 1, file);
    buffer[file_size] = '\0';
    fclose(file);

    // Mencari posisi awal dan akhir kolom yang akan dihapus
    char* start = strstr(buffer, columnName);
    if (start == NULL) {
        printf("Kolom %s tidak ditemukan dalam tabel %s.\n", columnName, tableName);
        free(buffer);
        return;
    }
    char* end = strchr(start, ',');
    if (end == NULL) {
        end = strchr(start, '\0');
    }

    // Menghapus kolom dari buffer
    memmove(start, end + 1, strlen(end + 1) + 1);

    // Menulis kembali konten buffer ke dalam file
    file = fopen(path, "w");
    if (file == NULL) {
        printf("Tidak dapat membuka file %s untuk ditulis.\n", path);
        free(buffer);
        return;
    }
    fwrite(buffer, strlen(buffer), 1, file);
    fclose(file);

    free(buffer);

    printf("Kolom %s telah dihapus dari tabel %s.\n", columnName, tableName);
}

void logCommand(char *username, char *command) {
    time_t rawtime;
    struct tm *timeinfo;
    char timestamp[20];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", timeinfo);

    FILE *logFile = fopen("command_log.txt", "a");
    if (logFile != NULL) {
        if (strcmp(username, "root") == 0) {
            fprintf(logFile, "%s:root:%s\n", timestamp, command);
        } else {
            fprintf(logFile, "%s:%s:%s\n", timestamp, username, command);
        }
        fclose(logFile);
    } else {
        printf("Failed to open log file.\n");
    }
}


int useData(char* username, char* database){

	char query[MAX_LENGTH];
	char newdatabase[MAX_LENGTH];
	char table[MAX_LENGTH];
	char column[MAX_LENGTH];
        // Membuka file login.txt
        FILE *file = fopen("login.txt", "r");
        if (file != NULL) {

             char line[MAX_LENGTH];
	     size_t len = strcspn(database, ";");
    	     database[len] = '\0';

             // Membaca setiap baris dalam file
             while (fgets(line, sizeof(line), file)) {
                 // Memisahkan username dan database dalam baris
                 char *fileUsername = strtok(line, ";");
                 char *fileDatabase = strtok(NULL, "\n");

                 // Memeriksa apakah username dan database cocok
                 if (fileUsername != NULL && fileDatabase != NULL &&
                     strcmp(username, fileUsername) == 0 && strcmp(database, fileDatabase) == 0) {

	             fgets(query, sizeof(query), stdin);

  		     // Remove trailing newline character
		     query[strcspn(query, "\n")] = '\0';

		     if (strncmp(query, "CREATE TABLE", 12) == 0) {
                        logCommand(username, query);
		        createTable(query, database);
		     }
                     else if (sscanf(query, "DROP TABLE %s", table) == 1){
			logCommand(username, query);
			dropTable(table, database);
                     }
                     else if (sscanf(query, "DROP COLUMN %s FROM %s", column, table) == 2) {
			logCommand(username, query);
                        dropColumn(column, table, database);
                     }

                     fclose(file);
                     return 0;
                 }
             }

             fclose(file);
         }
}

void createdatabase( char* user,  char* database) {
    // Membuka file login.txt dalam mode append ("a")
    FILE* file = fopen("login.txt", "a");

    if (file == NULL) {
        printf("Gagal membuka file login.txt.\n");
        exit(1);
    }
    size_t len = strcspn(database, ";");
    database[len] = '\0';

    // Menambahkan nama_database ke nama_user dalam format yang diinginkan
    fprintf(file, "%s;%s\n", user, database);

    // Menutup file
    fclose(file);

    // Create directory using system command
    char command[100];
    sprintf(command, "mkdir ./databases/%s", database);
    int status = system(command);
}

// Fungsi untuk menghapus direktori
void dropDatabase(const char* databaseName) {
    char command[MAX_LENGTH + 10];
    sprintf(command, "rm -r databases/%s", databaseName);
    system(command);
    printf("Database %s dihapus.\n", databaseName);
}

int main(int argc, char* argv[]) {
    char input[MAX_LENGTH];
    char nama_user[MAX_LENGTH];
    char password_user[MAX_LENGTH];
    char* username = NULL;
    char* password = NULL;
    char database[MAX_LENGTH];

    if (geteuid() == 0){

    fgets(input, sizeof(input), stdin);

    // Memeriksa format perintah
    if (sscanf(input, "CREATE USER %s IDENTIFIED BY %s", nama_user, password_user) == 2) {
	registerUser(nama_user, password_user);
	logCommand("root", input);
    }
    else if (sscanf(input, "GRANT PERMISSION %s INTO %s", database, nama_user) == 2) {
        grantPermission(database, nama_user);
	logCommand("root", input);
    }
    }
    else{
    for (int i = 1; i < argc; i += 2) {
        if (strcmp(argv[i], "-u") == 0) {
            username = argv[i + 1];
        } else if (strcmp(argv[i], "-p") == 0) {
            password = argv[i + 1];
        }
    }

    // Memeriksa apakah argumen username dan password sudah ada
    if (username == NULL || password == NULL) {
        printf("Username atau password tidak ditemukan.\n");
        return 1;
    }
	loginUser(username, password);

    if(BerhasilLogin == 1){

        fgets(input, sizeof(input), stdin);

        if (sscanf(input, "USE %s", database) == 1) {
            useData(username, database);
	    logCommand(username, input);
        }
        else if (sscanf(input, "CREATE DATABASE %s", database) == 1) {
            createdatabase(username, database);
	    logCommand(username, input);
        }
        else if (sscanf(input, "DROP DATABASE %s", database) == 1) {
            dropDatabase(database);
	    logCommand(username, input);
        }
    }
    }
    return 0;
}
